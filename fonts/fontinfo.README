All fonts are simply subdirectories below the directory of the
fontinfo file.  The fontinfo file determines what files (relative to
that directory) represent what characters.  It also defines a few
metrics.  I will quickly explain how it works.

The first line.

size                   45 0

starts with the word "size".  The next word is the size of the font in
pixels (height from lowest to highest point in the font).  The second
number is that font's orientation. 0 = left to right, 1 = from top to
bottom, 2 = from bottom to top and 3= form right to left. (upside-down
too).

The next line is the character (Use 0 if no character is defined for
that char (in this case if the char isn't in the font a space is
printed instead = ascii 32)

default                32

This defines the highest character value in that font (in this case
126, but the highest value is about 2-billion.  It is NOT a good idea
to have this higher than the maximum value anyway...)

max                    126

Now comes a line per character to define its geometry. the first thing
on the line is the ASCII value of that char (in this case 32)

32  45/space.tif       0  0  18

The second thing on the line is the image file for that character
RELATIVe to this font's dir (in this case in the 45 dir there is a
!.tif file)

33  45/!.tif           0  0  24

The third thing is the x offset inside that font's character box the image
is to be placed, the fourth being the y position for the image in the box
and the third being the widget of the box. Here is an example:

+---------------+
|    A          | A
|    |          | |
|    Y          | Size
|    |          | |
|    V          | |
|     +--+      | |
|<-X->|  |      | |
|     |  |      | |
|     +--+      | V
+---------------+
 <----Width---->

34  45/".tif           0  0  27

See above line too...

That's all there is to it.  Just make multiple character entries for
that font size.  When you've done that size, you can specify another
font size by starting again from the top on the next line like at the
end of this file.  See the fontinfo files already installed for a
working example of a single-size specified font.  The idea of multiple
sizes is fnlib will choose the best size to scale the pixmaps from.
So you make several sizes from very small (for miniature fonts) to
large (for big versions) then the fonts will always look decent.  The
more size versions you make the better the font will look, but the
more memory it will take.... :)

35  45/#.tif           0  6  30
36  45/$.tif           0  0  19
37  45/%.tif           0  0  42
... etc.
size 60 0
default 32
max 126
32  60/space.tif       0  0  28
.... etc.
